package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.category.IntegrationCategory;

@Category(IntegrationCategory.class)
public final class UserEndpointTest extends AbstractIntegrationTest {

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpointService().getAdminUserEndpointPort();

    @Test
    public void testUpdateUser() throws AbstractException_Exception {
        @NotNull final String newEmail = "new@new.ru";
        @NotNull final String newFirstName = "Ivan";
        @NotNull final String newLastName = "Ivanov";
        @NotNull final String newMiddleName = "Ivanovich";
        @NotNull final User user = userEndpoint.updateUser(
                userSession,
                newEmail,
                newFirstName,
                newLastName,
                newMiddleName
        );
        Assert.assertEquals(newEmail, user.getEmail());
        Assert.assertEquals(newFirstName, user.getFirstName());
        Assert.assertEquals(newLastName, user.getLastName());
        Assert.assertEquals(newMiddleName, user.getMiddleName());

        @Nullable final User userInSystem = sessionEndpoint.getUser(userSession);
        Assert.assertNotNull(userInSystem);
        Assert.assertEquals(newEmail, userInSystem.getEmail());
        Assert.assertEquals(newFirstName, userInSystem.getFirstName());
        Assert.assertEquals(newLastName, userInSystem.getLastName());
        Assert.assertEquals(newMiddleName, userInSystem.getMiddleName());
    }

    @Test
    public void testUpdatePassword() throws AbstractException_Exception {
        @NotNull final String password = "test";
        @NotNull final String newPassword = "test1";
        @NotNull final User user = userEndpoint.updateUserPassword(
                userSession,
                password,
                newPassword
        );
        Assert.assertNotNull(user);
        @Nullable final Session sessionIncorrect = sessionEndpoint.openSession("test", password);
        Assert.assertNull(sessionIncorrect);
        @Nullable final Session session = sessionEndpoint.openSession("test", newPassword);
        Assert.assertNotNull(session);
        userEndpoint.updateUserPassword(
                userSession,
                newPassword,
                password
        );
    }

    @Test
    public void testRegistryUser() throws AbstractException_Exception {
        @NotNull final String newUserLogin = "test1";
        @NotNull final String newUserPassword = "test1";
        @NotNull final Result result = userEndpoint.registryUser(newUserLogin, newUserPassword, "3@3.ru");
        Assert.assertNotNull(result);
        Assert.assertTrue(result.success);
        @NotNull final Session session = sessionEndpoint.openSession(newUserLogin, newUserPassword);
        Assert.assertNotNull(session);
        adminUserEndpoint.removeUserByLogin(adminSession, newUserLogin);
    }

}
