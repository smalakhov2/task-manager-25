package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.malakhov.tm.category.IntegrationCategory;

import java.util.List;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest extends AbstractIntegrationTest {

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Before
    public void before() throws AccessDeniedException_Exception {
        projectEndpoint.createProject(userSession, "Project1", null);
        projectEndpoint.createProject(userSession, "Project2", null);
        projectEndpoint.createProject(adminSession, "AdminProject1", null);
    }

    @After
    public void after(){
        projectEndpoint.clearAllProject(adminSession);
    }

    @Test
    public void testCreateProject() throws Exception {
        @NotNull final String newProjectName = "New project";
        @NotNull final String newProjectDescription = "New project";
        @NotNull final Result result = projectEndpoint.createProject(userSession, newProjectName, newProjectDescription);
        Assert.assertNotNull(result);
        Assert.assertTrue(result.success);
        @Nullable final Project project = projectEndpoint.getProjectByName(userSession, newProjectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(newProjectName, project.getName());
        Assert.assertEquals(newProjectDescription, project.getDescription());
    }

    @Test
    public void testClearProjectList() throws Exception {
        projectEndpoint.clearProjectList(userSession);
        @NotNull final List<Project> projects = projectEndpoint.getProjectList(userSession);
        Assert.assertEquals(0 , projects.size());
    }

    @Test
    public void testGetProjectList() throws Exception {
        @NotNull final List<Project> userProjects = projectEndpoint.getProjectList(userSession);
        Assert.assertEquals(2 , userProjects.size());
        @NotNull final List<Project> adminProjects = projectEndpoint.getProjectList(adminSession);
        Assert.assertEquals(1 , adminProjects.size());
    }

    @Test
    public void testRemoveProjectById() throws Exception {
        @NotNull final Project project = projectEndpoint.getProjectByName(userSession, "Project1");
        projectEndpoint.removeProjectById(userSession, project.id);
        @NotNull final List<Project> userProjects = projectEndpoint.getProjectList(userSession);
        Assert.assertEquals(1 , userProjects.size());
    }

    @Test
    public void testRemoveProjectByIndex() throws Exception {
        projectEndpoint.removeProjectByIndex(userSession, 0);
        @NotNull final List<Project> userProjects = projectEndpoint.getProjectList(userSession);
        Assert.assertEquals(1 , userProjects.size());
    }

    @Test
    public void testRemoveProjectsByName() throws Exception {
        projectEndpoint.removeProjectByName(userSession, "Project1");
        @NotNull final List<Project> userProjects = projectEndpoint.getProjectList(userSession);
        Assert.assertEquals(1 , userProjects.size());
    }

    @Test
    public void testGetProjectById() throws Exception {
        @NotNull final String id = projectEndpoint.getProjectByName(userSession, "Project1").id;
        @NotNull final Project project = projectEndpoint.getProjectById(userSession, id);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.name, "Project1");
    }

    @Test
    public void testGetProjectByIndex() throws Exception {
        @NotNull final Project project = projectEndpoint.getProjectByIndex(userSession, 0);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.name, "Project1");
    }

    @Test
    public void testGetProjectByName() throws Exception {
        @NotNull final Project project = projectEndpoint.getProjectByName(userSession, "Project1");
        Assert.assertNotNull(project);
        Assert.assertEquals(project.name, "Project1");
    }

    @Test
    public void testUpdateProjectById() throws Exception {
        @NotNull final String newName = "New name";
        @NotNull final String newDescription = "New description";
        @NotNull final String id = projectEndpoint.getProjectByName(userSession, "Project1").id;
        @Nullable final Project updatedProject = projectEndpoint.updateProjectById(
                userSession,
                id,
                newName,
                newDescription
        );
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(updatedProject.name, newName);
        Assert.assertEquals(updatedProject.description, newDescription);
        @NotNull final Project updatedProjectInSystem = projectEndpoint.getProjectById(userSession, id);
        Assert.assertEquals(updatedProjectInSystem.name, newName);
        Assert.assertEquals(updatedProjectInSystem.description, newDescription);
    }

    @Test
    public void testUpdateProjectByIndex() throws Exception {
        @NotNull final String newName = "New name";
        @NotNull final String newDescription = "New description";
        @Nullable final Project updatedProject = projectEndpoint.updateProjectByIndex(
                userSession,
                0,
                newName,
                newDescription
        );
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(updatedProject.name, newName);
        Assert.assertEquals(updatedProject.description, newDescription);
        @NotNull final Project updatedProjectInSystem = projectEndpoint.getProjectByIndex(userSession, 0);
        Assert.assertEquals(updatedProjectInSystem.name, newName);
        Assert.assertEquals(updatedProjectInSystem.description, newDescription);
    }

    @Test
    public void testClearAllProjects() throws Exception {
        @NotNull final Result result = projectEndpoint.clearAllProject(adminSession);
        Assert.assertTrue(result.success);
        @NotNull final List<Project> userProjects = projectEndpoint.getProjectList(userSession);
        Assert.assertEquals(0 , userProjects.size());
        @NotNull final List<Project> adminProjects = projectEndpoint.getProjectList(adminSession);
        Assert.assertEquals(0 , adminProjects.size());
    }

    @Test
    public void testGetAllProjectList() throws AbstractException_Exception {
        @NotNull final List<Project> tasks = projectEndpoint.getAllProjectList(adminSession);
        Assert.assertEquals(3, tasks.size());
    }

}