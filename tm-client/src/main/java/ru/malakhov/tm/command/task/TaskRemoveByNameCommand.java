package ru.malakhov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.endpoint.Task;
import ru.malakhov.tm.util.TerminalUtil;

public final class TaskRemoveByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[REMOVE TASK]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().removeTaskByName(session, name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }


    @Override
    public boolean secure() {
        return true;
    }

}
