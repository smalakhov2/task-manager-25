package ru.malakhov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IServiceLocator;

@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute() throws Exception;

    public abstract boolean secure();

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final StringBuilder stringBuilder = new StringBuilder();
        @Nullable final String name = name();
        if (!name.isEmpty()) stringBuilder.append(name);
        @Nullable final String arg = arg();
        if (arg != null && !arg.isEmpty()) stringBuilder.append(", ").append(arg);
        @Nullable final String description = description();
        if (description != null && !description.isEmpty()) stringBuilder.append(": ").append(description);
        return stringBuilder.toString();
    }

}
