package ru.malakhov.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Session;

public final class LogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        serviceLocator.getSessionEndpoint().closeSession(session);
        serviceLocator.getPropertyService().setSession(null);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
