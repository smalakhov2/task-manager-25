package ru.malakhov.tm.command.admin.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Session;

import java.io.IOException;

public final class DataBase64ClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove Base64 data.";
    }

    @Override
    public void execute() throws IOException, AbstractException_Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        serviceLocator.getAdminDataEndpoint().clearDataBase64(session);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
