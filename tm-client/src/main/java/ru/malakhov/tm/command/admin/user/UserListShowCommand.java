package ru.malakhov.tm.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.endpoint.User;

import java.util.List;

public final class UserListShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user list.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[USER-LIST]");
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        @Nullable final List<User> users = serviceLocator.getAdminUserEndpoint().getAllUserList(session);
        if (users == null) {
            System.out.println("[FAIL]");
            return;
        }
        int index = 1;
        for (@Nullable final User user : users) {
            if (user != null) {
                System.out.println(index + ". " + user.getLogin());
                index++;
            }
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}
