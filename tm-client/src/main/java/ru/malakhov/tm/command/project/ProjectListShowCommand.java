package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Project;
import ru.malakhov.tm.endpoint.Session;

import java.util.List;

public final class ProjectListShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[LIST PROJECTS]");
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        @NotNull final List<Project> projects = serviceLocator.getProjectEndpoint().getProjectList(session);
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
