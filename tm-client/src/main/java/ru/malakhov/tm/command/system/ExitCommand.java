package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Exit the program.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public boolean secure() {
        return false;
    }

}
