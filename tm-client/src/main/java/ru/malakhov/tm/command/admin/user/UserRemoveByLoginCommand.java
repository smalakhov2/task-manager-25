package ru.malakhov.tm.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Session;
import ru.malakhov.tm.endpoint.User;
import ru.malakhov.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-remove";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[USER-REMOVE]");
        System.out.print("ENTER USER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final Session session = serviceLocator.getPropertyService().getSession();
        @Nullable final User currentUser = serviceLocator.getSessionEndpoint().getUser(session);
        if (currentUser == null) {
            System.out.println("[Error]");
            return;
        }
        if (login.equals(currentUser.getLogin())) {
            String answer = "";
            do {
                System.out.print("THIS IS YOUR ACCOUNT. DELETE IT(Y/N): ");
                answer = TerminalUtil.nextLine().toLowerCase();
            } while (!answer.equals("y") && !answer.equals("n"));
            if (answer.equals("n")) {
                System.out.println("[CANCEL]");
                return;
            }
            serviceLocator.getPropertyService().setSession(null);
        }
        @Nullable final User user = serviceLocator.getAdminUserEndpoint().removeUserByLogin(
                session,
                login
        );
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
