package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Sergei Malakhov");
        System.out.println("smalakhov2@rencredit.ru");
    }

    @Override
    public boolean secure() {
        return false;
    }

}
