package ru.malakhov.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IPropertyRepository;
import ru.malakhov.tm.endpoint.Session;

public final class PropertyRepository implements IPropertyRepository {

    @Nullable
    private Session session;

    @Nullable
    @Override
    public Session getSession() {
        return session;
    }

    @Override
    public void setSession(@Nullable final Session session) {
        this.session = session;
    }

}