package ru.malakhov.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.ISqlSessionProvider;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.api.service.ISqlSessionService;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public final class SqlSessionService implements ISqlSessionService, ISqlSessionProvider {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private EntityManagerFactory entityManagerFactory;

    public SqlSessionService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, serviceLocator.getPropertyService().getJdbcDriver());
        settings.put(Environment.URL, serviceLocator.getPropertyService().getJdbcUrl());
        settings.put(Environment.USER, serviceLocator.getPropertyService().getJdbcUsername());
        settings.put(Environment.PASS, serviceLocator.getPropertyService().getJdbcPassword());
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(TaskDto.class);
        sources.addAnnotatedClass(ProjectDto.class);
        sources.addAnnotatedClass(UserDto.class);
        sources.addAnnotatedClass(SessionDto.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    public void initFactory() {
        entityManagerFactory = factory();
    }

    public void closeFactory() {
        entityManagerFactory.close();
    }

    @NotNull
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
