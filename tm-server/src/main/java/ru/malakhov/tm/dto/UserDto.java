package ru.malakhov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.util.HashUtil;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto extends AbstractEntityDto {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "login")
    private String login = "";

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash = "";

    @NotNull
    @Column(name = "email")
    private String email = "";

    @Nullable
    @Column(name = "first_name")
    private String firstName = "";

    @Nullable
    @Column(name = "last_name")
    private String lastName = "";

    @Nullable
    @Column(name = "middle_name")
    private String middleName = "";

    @NotNull
    @Column(name = "locked")
    private Boolean locked = false;

    @NotNull
    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;

    public UserDto(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email
    ) {
        this.login = login;
        this.email = email;
        this.passwordHash = HashUtil.salt(password);
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof UserDto && super.equals(o)) {
            @NotNull final UserDto user = (UserDto) o;
            return Objects.equals(login, user.login)
                    && Objects.equals(email, user.email)
                    && Objects.equals(passwordHash, user.passwordHash);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, email, passwordHash);
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    public void setLogin(@NotNull String login) {
        this.login = login;
    }

    @NotNull
    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(@NotNull String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NotNull String email) {
        this.email = email;
    }

    @Nullable
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@Nullable String firstName) {
        this.firstName = firstName;
    }

    @Nullable
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@Nullable String lastName) {
        this.lastName = lastName;
    }

    @Nullable
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(@Nullable String middleName) {
        this.middleName = middleName;
    }

    @NotNull
    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(@NotNull Boolean locked) {
        this.locked = locked;
    }

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setRole(@NotNull Role role) {
        this.role = role;
    }


}