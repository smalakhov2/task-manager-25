package ru.malakhov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull
    @WebMethod
    Result updateUser(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "email") @Nullable String email,
            @WebParam(name = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName") @Nullable String middleName
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result updateUserPassword(
            @WebParam(name = "session") @Nullable SessionDto session,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "newPassword") @Nullable String newPassword
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Result registryUser(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "email") @Nullable String email
    ) throws AbstractException;

}