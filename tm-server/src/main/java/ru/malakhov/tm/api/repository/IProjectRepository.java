package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<ProjectDto> {

    @NotNull
    List<ProjectDto> findAllDto();

    @NotNull
    List<Project> findAllEntity();

    @NotNull
    List<ProjectDto> findAllDtoByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAllEntityByUserId(@NotNull String userId);

    @Nullable
    ProjectDto findOneDtoById(@NotNull String id);

    @Nullable
    Project findOneEntityById(@NotNull String id);

    @Nullable
    ProjectDto findOneDtoById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findOneEntityById(@NotNull String userId, @NotNull String id);

    @Nullable
    ProjectDto findOneDtoByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneEntityByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    ProjectDto findOneDtoByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project findOneEntityByName(@NotNull String userId, @NotNull String name);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    void removeOneById(@NotNull String id);

    void removeOneById(@NotNull String userId, @NotNull String id);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByName(@NotNull String userId, @NotNull String name);

}