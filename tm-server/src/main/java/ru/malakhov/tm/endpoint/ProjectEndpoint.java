package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.IProjectEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.response.Fail;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.dto.response.Success;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearAllProject(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getProjectService().removeAll();
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDto> getAllProjectList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().findAllDto();
    }

    @NotNull
    @Override
    @WebMethod
    public Result createProject(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getProjectService().create(session.getUserId(), name, description);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearProjectList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getProjectService().removeAllByUserId(session.getUserId());
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDto> getProjectList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllDtoByUserId(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Result removeProjectById(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public Result removeProjectByIndex(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws AbstractException {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public Result removeProjectByName(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "name") @Nullable final String name
    ) throws AbstractException {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDto getProjectById(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneDtoById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDto getProjectByIndex(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneDtoByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDto getProjectByName(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneDtoByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Result updateProjectById(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getProjectService().updateProjectById(
                    session.getUserId(),
                    id,
                    name,
                    description
            );
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result updateProjectByIndex(
            @WebParam(name = "session") @Nullable final SessionDto session,
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        try {
            serviceLocator.getSessionService().validate(session);
            serviceLocator.getProjectService().updateProjectByIndex(
                    session.getUserId(),
                    index,
                    name,
                    description
            );
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

}