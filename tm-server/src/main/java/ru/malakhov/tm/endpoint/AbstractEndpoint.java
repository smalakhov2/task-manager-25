package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.endpoint.IEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;

public class AbstractEndpoint implements IEndpoint {

    @NotNull
    protected final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}