package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public final class EmptyFilePathException extends AbstractException {

    public EmptyFilePathException() {
        super("Error! File path is empty...");
    }

}
