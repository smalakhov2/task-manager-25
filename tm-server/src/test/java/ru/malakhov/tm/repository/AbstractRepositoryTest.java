package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.DisableOnDebug;
import org.junit.rules.Timeout;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.exception.empty.EmptyIdException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class AbstractRepositoryTest extends AbstractDataTest {

    @Rule
    public DisableOnDebug debugTime = new DisableOnDebug(Timeout.seconds(30));

    @NotNull
    private final IProjectService projectService = bootstrap.getProjectService();

    @NotNull
    private final ProjectDto projectOne = new ProjectDto("Project1", "", userDto.getId());

    @NotNull
    private final ProjectDto projectTwo = new ProjectDto("Project2", "", userDto.getId());

    @NotNull
    private final ProjectDto projectThree = new ProjectDto("Project3", "", userDto.getId());

    @NotNull
    private final ProjectDto unknownProject = new ProjectDto("Unknown", "", unknownUserDto.getId());

    @NotNull
    private final List<ProjectDto> allProjects = new ArrayList<>(Arrays.asList(projectOne, projectTwo, projectThree));

    private void loadData() {
        bootstrap.getProjectService().persist(allProjects);
    }
    
    @NotNull
    private IProjectRepository getRepository() {
        return bootstrap.getProjectService().getRepository();
    }

    public AbstractRepositoryTest() throws Exception {
        super();
    }

    @Before
    public void before() {
        bootstrap.getUserService().merge(userDto, adminDto);
    }

    @After
    public void after() {
        bootstrap.getProjectService().removeAll();
        bootstrap.getUserService().removeOne(userDto);
        bootstrap.getUserService().removeOne(adminDto);
    }

    @Test
    public void testPersist() {
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.persist(projectOne);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }

        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void testPersistCollection() {
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.persist(allProjects);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }

        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
    }

    @Test
    public void testPersistVarargs() {
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.persist(projectOne, projectThree, projectTwo);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }

        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
    }

    @Test
    public void testPersistVarargsWithNull() {
        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.persist(null, projectThree, null);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }

        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void testMerge() throws EmptyIdException {
        loadData();
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        projectOne.setName(newName);
        projectOne.setDescription(newDescription);

        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.merge(projectOne);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }

        @Nullable final ProjectDto project = projectService.findOneDtoById(projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project, projectOne);
    }

    @Test
    public void testMergeCollection() {
        loadData();
        @NotNull final String newName = "new name1";
        @NotNull final String newDescription = "new description1";
        for (@NotNull final ProjectDto project : allProjects) {
            project.setName(newName);
            project.setDescription(newDescription);
        }

        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.merge(allProjects);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }

        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
        for (@NotNull final ProjectDto project : projects) {
            Assert.assertEquals(project.getName(), newName);
            Assert.assertEquals(project.getDescription(), newDescription);
        }
    }

    @Test
    public void testMergeVarargs() {
        loadData();
        @NotNull final String newName = "new name2";
        @NotNull final String newDescription = "new description2";
        for (@NotNull final ProjectDto project : allProjects) {
            project.setName(newName);
            project.setDescription(newDescription);
        }

        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.merge(projectOne, projectTwo, projectThree);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }

        @NotNull final List<ProjectDto> projects = projectService.findAllDto();
        Assert.assertEquals(3, projects.size());
        for (@NotNull final ProjectDto project : projects) {
            Assert.assertEquals(project.getName(), newName);
            Assert.assertEquals(project.getDescription(), newDescription);
        }
    }

    @Test
    public void testMergeVarargsWithNull() throws EmptyIdException {
        loadData();
        @NotNull final String newName = "new name3";
        @NotNull final String newDescription = "new description3";
        projectTwo.setName(newName);
        projectTwo.setDescription(newDescription);

        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.merge(null, projectTwo, null);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }

        @Nullable final ProjectDto project = projectService.findOneDtoById(projectTwo.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project, projectTwo);
    }

    @Test
    public void testRemoveOne() throws EmptyIdException {
        loadData();

        @NotNull final IProjectRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOne(projectOne);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }

        @Nullable final ProjectDto project = projectService.findOneDtoById(projectOne.getId());
        Assert.assertNull(project);
    }

}
